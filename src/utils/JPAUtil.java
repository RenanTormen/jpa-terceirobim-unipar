package utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;

	private static void inicializarBanco() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("TerceiroBimestrePU");
			entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static EntityManager getEntityManager() {
		return entityManager;
	}

	public static <T> void persistir(T entidade) {
		EntityManager entityManager = getEntityManager();
		if (entityManager == null) {
			inicializarBanco();
		}

		try {
			entityManager.getTransaction().begin();
			entityManager.persist(entidade);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void main(String[] args) {
		JPAUtil.inicializarBanco();
	}

}
